provider "google" {
  credentials = file("../../credentials.json")
  project     = var.project_id
  region      = var.region
  version     = "~> 2.7.0"
}

resource "google_compute_address" "ip_address" {
  name = "external-ip"
}

locals {
  access_config = {
    nat_ip       = google_compute_address.ip_address
    network_tier = "PREMIUM"
  }
}

# Create instances options
module "compute_instance" {
  source        = "../../modules/compute_instance"
  subnetwork    = var.subnetwork
  num_instances = var.num_instances
  hostname      = "instance-simple"
  network       = "default"
}
