locals {
  hostname      = var.hostname == "" ? "default" : var.hostname
  num_instances = length(var.static_ips) == 0 ? var.num_instances : length(var.static_ips)

  # local.static_ips is the same as var.static_ips with a dummy element appended
  # at the end of the list to work around "list does not have any elements so cannot
  # determine type" error when var.static_ips is empty
  static_ips = concat(var.static_ips, ["NOT_AN_IP"])
}

# Data Sources
data "google_compute_zones" "available" {
}

data "google_compute_image" "image" {
  project = var.source_image != "" ? var.source_image_project : "centos-cloud"
  name    = var.source_image != "" ? var.source_image : "centos-7-v20191014"
}

data "google_compute_image" "image_family" {
  project = var.source_image_family != "" ? var.source_image_project : "centos-cloud"
  family  = var.source_image_family != "" ? var.source_image_family : "centos-7"
}

# Instances
resource "google_compute_instance" "instance" {
  provider     = google
  count        = local.num_instances
  name         = "${local.hostname}-${format("%03d", count.index + 1)}"
  machine_type = var.machine_type
  zone         = data.google_compute_zones.available.names[count.index % length(data.google_compute_zones.available.names)]
  tags         = var.tags
  boot_disk {
    initialize_params {
      size  = var.disk_size_gb
      image = var.source_image != "" ? data.google_compute_image.image.self_link : data.google_compute_image.image_family.self_link
      type  = var.disk_type
    }
  }

  network_interface {
    network            = var.network
    subnetwork         = var.subnetwork
    subnetwork_project = var.subnetwork_project
    network_ip         = length(var.static_ips) == 0 ? "" : element(local.static_ips, count.index)
    dynamic "access_config" {
      for_each = var.access_config
      content {
        nat_ip       = access_config.value.nat_ip
        network_tier = access_config.value.network_tier
      }
    }
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }
}
